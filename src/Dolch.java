import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;
import java.util.Scanner;


public class Dolch {
    protected int schaden = 5;
    protected String name = "Dolch";


    public Dolch()
    {
        this.name = name;
        this.schaden = schaden;
    }

    public int getSchaden() {
        return schaden;
    }

    public String getName() {
        return name;
    }

    public String toString()
    {
        return getName();
    }
}