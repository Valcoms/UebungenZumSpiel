import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;
import java.util.Scanner;


public class Eingabe {
    protected String eingabeString;
    protected String yesAngabe;
    //protected ArrayList <String> yesListe;


    public Eingabe()
    {

    }


    public void input() {
        Scanner eingabe = new Scanner(System.in);
        System.out.println("Willst du einen Charakter erstellen ? \n[yes] oder [no]\n");
        String enterEingabe = eingabe.next();

        //YES LISTE
        String[] yesListe = {"y", "YES", "yes", "Ja", "j", "Y", "J"};

        //Ob irgendeine Eingabe in der Liste iST !boolean true | false inputInArrayYes = Arrays.stream(yesListe).anyMatch(enterEingabe::equals);
        boolean inputInArrayYes = Arrays.stream(yesListe).anyMatch(enterEingabe::equals);
        //NO LISTE
        String[] noListe = {"n", "NO", "no", "nein", "N"};
        boolean inputInArrayNo = Arrays.stream(noListe).anyMatch(enterEingabe::equals);

        Boolean programmLeauft = true;

        while (programmLeauft)
            if (inputInArrayYes) {


                System.out.println("Erstellen:\nGib den Namen deines Charakters ein: ");
                String enterName = eingabe.next();
                charErstellen(enterName);
                programmLeauft = false;

            } else if(inputInArrayNo)
            {
                System.out.println("Dann eben nicht!");
                programmLeauft = false;
            }

            else {
                System.out.println("Falsch gib einer der folgenden Zeichen ein!");
            }

    }



    public void charErstellen(String name)
    {
        System.out.println("\nDein Name lautet " + name);
    }



}
