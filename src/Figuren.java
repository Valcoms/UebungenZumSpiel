import java.util.ArrayList;

public class Figuren {

    protected int lebenspunkte;
    protected String name;
    protected int schaden;
    protected ArrayList<Object> figuren;

    public Figuren()
    {
        this.lebenspunkte = lebenspunkte;
        this.name = name;
        this.schaden = schaden;
        this.figuren = new ArrayList<Object>();

    }

    public int getLebenspunkte() {
        return lebenspunkte;
    }

    public void setLebenspunkte(int lebenspunkte) {
        this.lebenspunkte = lebenspunkte;
    }

    public int getSchaden() {
        return schaden;
    }

    public void setSchaden(int schaden) {
        this.schaden = schaden;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void veringereLeben(int schaden)
    {
        this.lebenspunkte = this.lebenspunkte - schaden;
        if(this.lebenspunkte < 0)
        {
            this.lebenspunkte = 0;
        }
    }

    public ArrayList<Object> getFiguren() {
        return figuren;
    }

    public void setFiguren(ArrayList<Object> figuren) {
        this.figuren = figuren;

    }
}
