import java.util.Scanner;

public class Krieger {
    protected String name;
    protected String aussehen;
    protected String waffe;
    protected String ruestung;
    protected int lebenspunkte;
    protected int alter;
    protected Schwert schwert;
    protected int schaden;

    public Krieger(String name, int lebenspunkte)
    {
        this.name = name;
        this.aussehen = aussehen;
        this.waffe = waffe;
        this.ruestung = ruestung;
        this.lebenspunkte = lebenspunkte;
        this.alter = alter;
        this.schaden = 12;


    }

    public void buildKrieger()
    {
//        Scanner eingabe = new Scanner(System.in);
//        System.out.println("> Name: ");
//        String nameEingabe = eingabe.next();
//        System.out.println("> Aussehen: ");
//        String aussehenEingabe = eingabe.next();
//        System.out.println("> Waffe: ");
//        String waffeEingabe = eingabe.next();
//        System.out.println("> Rüstung: ");
//        String ruestungEingabe = eingabe.next();
//
//
//        setName(nameEingabe);
//        setAussehen(aussehenEingabe);
//        setWaffe(waffeEingabe);
//        setRuestung(ruestungEingabe);
//        setAlter(22);
//
//        System.out.println("Dein Charakter: " + getName() + "\n" + getWaffe() + "\n" + getRuestung() + "\n" + getAussehen()  + "\n" + getAlter() + "\n" + getLebenspunkte());

    }
    //SETTER
    public void setName(String nameEingabe) {

        this.name = nameEingabe;
    }

    public void setAussehen(String aussehenEingabe) {
        this.aussehen = aussehenEingabe;
    }

    public void setWaffe(Schwert schwert) {
        this.schwert = schwert;
    }

    public void setRuestung(String ruestungEingabe) {
        this.ruestung = ruestungEingabe;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }


    //GETTER
    public String getName() {
        return name;
    }

    public int getSchaden() {
        return schaden;
    }
    public String getAussehen() {
        return aussehen;
    }

    public String getSchwert() {
        return waffe;
    }

    public String getRuestung() {
        return ruestung;
    }

    public int getLebenspunkte() {
        return lebenspunkte;
    }

    public int getAlter() {
        return alter;
    }

    public void veringereLeben(int schaden)

    {

        this.lebenspunkte = this.lebenspunkte - schaden;
        if(this.lebenspunkte < 0)
        {
            this.lebenspunkte = 0;
        }

    }
}
