import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;
import java.util.Scanner;


public class Langschwert {
    protected int schaden = 15;
    protected String name = "Langschwert";


    public Langschwert()
    {
        this.name = name;
        this.schaden = schaden;
    }

    public int getSchaden() {
        return schaden;
    }

    public String getName() {
        return name;
    }

    public String toString()
    {
        return getName();
    }
}