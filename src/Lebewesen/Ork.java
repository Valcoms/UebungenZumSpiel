package Lebewesen;

public class Ork {

        protected int lebenspunkte;
        protected int schaden;
        protected String name;

    public Ork(String name, int lebenspunkte, int schaden)
    {
            this.name = name;
            this.lebenspunkte = lebenspunkte;
            this.schaden = schaden;

    }

        public int getLebenspunkte() {
            return lebenspunkte;
        }

        public void setLebenspunkte(int lebenspunkte) {
            this.lebenspunkte = lebenspunkte;
        }

        public int getSchaden() {
            return schaden;
        }

        public void setSchaden(int schaden) {
            this.schaden = schaden;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void veringereLeben(int schaden)

        {

            this.lebenspunkte = this.lebenspunkte - schaden;
            if(this.lebenspunkte < 0)
            {
                this.lebenspunkte = 0;
            }

        }


}


