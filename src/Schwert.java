import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.*;
import java.util.Scanner;


public class Schwert {
    protected int schaden;
    protected String name;


    public Schwert()
    {
        this.name = "Schwert";
        this.schaden = 12;
    }

    public int getSchaden() {
        return schaden;
    }

    public String getName() {
        return name;
    }


    public String toString()
    {
        return getName();
    }
}
